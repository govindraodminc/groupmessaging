package com.example.contactgroup;

import com.example.contactgroup.helper.DatabaseHelper;
import com.example.contactgroup.helper.ObjectSerializer;
import com.example.contactgroup.model.Tag;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends Activity {

	private static final String SHARED_PREFS_FILE = "ID";
	private static final String TASKS = "ARRAY_ID";
	ListView listView;
	List<Tag> rowItems;
	Tag rowItem;
	DatabaseHelper db;
	ArrayList<Long> currentTasks;

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);
		db = new DatabaseHelper(getApplicationContext());

		List<Tag> allTags = db.getAllTags();

		CustomListViewAdapter adapter = new CustomListViewAdapter(this,
				R.layout.list_item, allTags);

		listView = (ListView) findViewById(R.id.listView1);
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@SuppressWarnings("rawtypes")
			public void onItemClick(AdapterView<?> av, View view, int i, long l) {
				SharedPreferences prefs = getSharedPreferences(
						SHARED_PREFS_FILE, Context.MODE_PRIVATE);
				try {
					currentTasks = (ArrayList<Long>) ObjectSerializer
							.deserialize(prefs.getString(TASKS,
									ObjectSerializer
											.serialize(new ArrayList<Long>())));

					for (Iterator iterator = currentTasks.iterator(); iterator
							.hasNext();) {
						Long tag = (Long) iterator.next();
						Log.i("ID_NO", tag + "");
					}
					Log.i("Size", currentTasks.size() + "");
				} catch (IOException e) {
					e.printStackTrace();
				}

				List<Tag> allTags = db.getAllTags();
				Tag str = allTags.get(i);
				Toast.makeText(getApplicationContext(), str.getTagName(),
						Toast.LENGTH_LONG).show();
				Intent intent = new Intent(getApplicationContext(),
						ContactActivity.class);
				intent.putExtra("Model", str.getTagName());
				if (!currentTasks.isEmpty())
					intent.putExtra("ID", currentTasks.get(i) + "");

				startActivity(intent);
			}
		});
		listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
		listView.setMultiChoiceModeListener(new ModeCallback());
		listView.setAdapter(adapter);

	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		getActionBar().setSubtitle("Long press to start selection");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main_activity_actions, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case R.id.action_add:
			openSearch();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void openSearch() {
		// TODO Auto-generated method stub
		showCustomDialog();
	}

	protected void showCustomDialog() {
		// TODO Auto-generated method stub

		final Dialog dialog = new Dialog(MainActivity.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_contact);

		final EditText editText = (EditText) dialog
				.findViewById(R.id.editText1);
		Button button = (Button) dialog.findViewById(R.id.button1);
		button.setOnClickListener(new View.OnClickListener() {
			Tag tag1;

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				tag1 = new Tag(editText.getText().toString());
				long tag_id = db.createTag(tag1);
				addTask(tag_id);
				List<Tag> allTags = db.getAllTags();
				CustomListViewAdapter adapter = new CustomListViewAdapter(
						getApplicationContext(), R.layout.list_item, allTags);
				listView.setAdapter(adapter);
				dialog.dismiss();
			}
		});

		dialog.show();
	}

	public void addTask(long t) {
		if (null == currentTasks) {
			currentTasks = new ArrayList<Long>();
		}
		currentTasks.add(t);

		// save the task list to preference
		SharedPreferences prefs = getSharedPreferences(SHARED_PREFS_FILE,
				Context.MODE_PRIVATE);
		Editor editor = prefs.edit();
		try {
			editor.putString(TASKS, ObjectSerializer.serialize(currentTasks));
		} catch (IOException e) {
			e.printStackTrace();
		}
		editor.commit();
	}

	private class ModeCallback implements ListView.MultiChoiceModeListener {

		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			MenuInflater inflater = getMenuInflater();
			inflater.inflate(R.menu.list_select_menu, menu);
			mode.setTitle("Select Items");
			return true;
		}

		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			return true;
		}

		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			switch (item.getItemId()) {
			case R.id.share:
				Toast.makeText(MainActivity.this,
						"Shared " + listView.getCheckedItemCount() + " items",
						Toast.LENGTH_SHORT).show();
				mode.finish();
				break;
			default:
				Toast.makeText(MainActivity.this, "Clicked " + item.getTitle(),
						Toast.LENGTH_SHORT).show();
				break;
			}
			return true;
		}

		public void onDestroyActionMode(ActionMode mode) {
		}

		public void onItemCheckedStateChanged(ActionMode mode, int position,
				long id, boolean checked) {
			final int checkedCount = listView.getCheckedItemCount();
			switch (checkedCount) {
			case 0:
				mode.setSubtitle(null);
				break;
			case 1:
				mode.setSubtitle("One item selected");
				break;
			default:
				mode.setSubtitle("" + checkedCount + " items selected");
				break;
			}
		}

	}
}