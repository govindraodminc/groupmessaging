package com.example.contactgroup;

import java.util.HashSet;
import java.util.Set;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class ContactPreferences {

	public ContactPreferences() {
		// TODO Auto-generated constructor stub
	}

	public static final String GROUP_BODY = "group";
	private static final String APP_SHARED_PREFS = ContactPreferences.class
			.getSimpleName(); // Name of the file -.xml
	private SharedPreferences _sharedPrefs;
	private Editor _prefsEditor;

	public ContactPreferences(Context context) {
		this._sharedPrefs = context.getSharedPreferences(APP_SHARED_PREFS,
				Activity.MODE_PRIVATE);
		this._prefsEditor = _sharedPrefs.edit();
	}

	public Set<String> getGroup() {
		Set<String> myStrings = _sharedPrefs.getStringSet(GROUP_BODY,
				new HashSet<String>());
		return myStrings;
	}

	public void saveGroup(String text) {
		Set<String> myStrings = _sharedPrefs.getStringSet(GROUP_BODY,
				new HashSet<String>());
		myStrings.add(text);
		// Save the list.
		_prefsEditor.putStringSet(GROUP_BODY, myStrings);
		_prefsEditor.commit();
	}
}
