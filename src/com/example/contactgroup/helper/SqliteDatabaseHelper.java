package com.example.contactgroup.helper;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SqliteDatabaseHelper extends SQLiteOpenHelper {

	// Database Version
	private static final int DATABASE_VERSION = 1;

	// Database Name
	private static final String DATABASE_NAME = "Contacts";

	// Table Names
	private static final String Group = "Group";
	private static final String Group_Contacts = "Group_Contacts";

	private static final String CREATE_TABLE_Group = "CREATE TABLE " + Group
			+ " (" + "group_id"
			+ " INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL  UNIQUE , "
			+ "group_name" + " VARCHAR NOT NULL )";

	private static final String CREATE_TABLE_Group_Contacts = "CREATE TABLE "
			+ Group_Contacts + " (" + "Group_Contacts_Id"
			+ " INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL  UNIQUE , "
			+ "Group_Contacts_Name" + " VARCHAR NOT NULL , "
			+ "Group_Contacts_GroupId" + " INTEGER NOT NULL , "
			+ "Group_Contacts_No" + " INTEGER NOT NULL )";

	public SqliteDatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		// creating required tables
		db.execSQL(CREATE_TABLE_Group);
		db.execSQL(CREATE_TABLE_Group_Contacts);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL("DROP TABLE IF EXISTS " + CREATE_TABLE_Group);
		db.execSQL("DROP TABLE IF EXISTS " + CREATE_TABLE_Group_Contacts);

		onCreate(db);
	}

	public int insertGroup(String GroupName) {
		String countQuery = "INSERT INTO " + Group + " (group_name) VALUES ("
				+ GroupName + ")";
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);

		int count = cursor.getCount();
		cursor.close();

		// return count
		return count;
	}

	public int insertContact(String ContactName, int GroupId, long PhoneNo) {

		String countQuery = "INSERT INTO "
				+ Group_Contacts
				+ " (Group_Contacts_Name,Group_Contacts_GroupId,Group_Contacts_No) VALUES ("
				+ ContactName + "," + GroupId + "," + PhoneNo + ")";
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);

		int count = cursor.getCount();
		cursor.close();

		// return count
		return count;
	}

	public int deleteGroupContact(int GroupId, int ContactId) {
		String countQuery = "DELETE FROM Group_Contacts where Group_Contacts_Id = "
				+ ContactId + " and Group_Contacts_GroupId = " + GroupId;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);

		int count = cursor.getCount();
		cursor.close();
		// return count
		return count;
	}
}
