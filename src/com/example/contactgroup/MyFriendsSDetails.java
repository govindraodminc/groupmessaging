package com.example.contactgroup;

public class MyFriendsSDetails {
	
	 private long todo_Id;
	 private String myfriendname=null;
	 private String myfriendnickname=null;
	 private int photo=0;
	 
	 public MyFriendsSDetails() {
		// TODO Auto-generated constructor stub
	}
	 
	 public MyFriendsSDetails(String friendname, String friendnickname, int myphoto){
		 this.myfriendname=friendname;
		 this.myfriendnickname=friendnickname;
		 this.photo=myphoto;
	 }

	public String getMyfriendname() {
		return myfriendname;
	}

	public void setMyfriendname(String myfriendname) {
		this.myfriendname = myfriendname;
	}

	public String getMyfriendnickname() {
		return myfriendnickname;
	}

	public void setMyfriendnickname(String myfriendnickname) {
		this.myfriendnickname = myfriendnickname;
	}

	public int getPhoto() {
		return photo;
	}

	public void setPhoto(int photo) {
		this.photo = photo;
	}

	public long getTodo_Id() {
		return todo_Id;
	}

	public void setTodo_Id(long todo_Id) {
		this.todo_Id = todo_Id;
	}

	 
}
