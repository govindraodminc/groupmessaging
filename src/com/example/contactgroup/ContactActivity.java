package com.example.contactgroup;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.example.contactgroup.helper.DatabaseHelper;
import com.example.contactgroup.model.Todo;
import com.javacodegeeks.android.androidphonecontactsexample.Contact;
import com.javacodegeeks.android.androidphonecontactsexample.MultipleListActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

@SuppressLint("NewApi")
public class ContactActivity extends Activity implements
		MultiChoiceModeListener {

	private String[] myfriendname = null;
	private String[] myfriendnickname = null;
	private int[] photo = null;
	ListView listView = null;
	Context contex = null;
	private ContactPreferences contactPreferences;
	MyListAdapter adapter = null;
	private List<MyFriendsSDetails> list = new ArrayList<MyFriendsSDetails>();
	MyFriendsSDetails friendsSDetails;
	DatabaseHelper db;
	List<Todo> tagsWatchList;
	String modelID;
	String modelName;

	@SuppressWarnings("static-access")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.contact_activity);
		
		getActionBar().setDisplayHomeAsUpEnabled(true);
		contex = this;
		db = new DatabaseHelper(getApplicationContext());
		modelName = getIntent().getStringExtra("Model");

		tagsWatchList = db.getAllToDosByTag(modelName);
		for (Todo todo : tagsWatchList) {
			friendsSDetails = new MyFriendsSDetails();
			Log.d("ToDo " + modelName, todo.getNote());
			friendsSDetails.setMyfriendname(todo.getNote());
			Log.i("Status", todo.getStatus() + "");
			friendsSDetails.setMyfriendnickname(todo.getStatus() + "");
			friendsSDetails.setPhoto(android.R.drawable.ic_menu_call);
			friendsSDetails.setTodo_Id(todo.getId());
			list.add(friendsSDetails);
		}

		listView = (ListView) findViewById(R.id.listview);
		modelID = getIntent().getStringExtra("ID");
		contactPreferences = new ContactPreferences(getApplicationContext());

		Log.i("Model", modelName);
		Log.i("ID", modelID);

		adapter = new MyListAdapter(contex, list);
		listView.setAdapter(adapter);
		listView.setMultiChoiceModeListener(this);
		listView.setChoiceMode(listView.CHOICE_MODE_MULTIPLE_MODAL);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main_activity_actions, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case R.id.action_add:
			openSearch();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void openSearch() {
		// TODO Auto-generated method stub
		// showCustomDialog();
		Intent i = new Intent(this, MultipleListActivity.class);
		startActivityForResult(i, 1);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 1) {
			if (resultCode == RESULT_OK) {
				Bundle b = data.getExtras();
				ArrayList<Contact> contacts = b
						.getParcelableArrayList("contacts");
				Todo todoSet;
				for (Iterator iterator = contacts.iterator(); iterator
						.hasNext();) {
					todoSet = new Todo();
					Contact contact = (Contact) iterator.next();
					todoSet.setNote(contact.getName());
					todoSet.setStatus(Long.parseLong(contact.getPh_no()
							.replace(" ", "")));
					long todo1_id = db.createToDo(todoSet,
							new long[] { Long.parseLong(modelID) });
					Log.i(getPackageName() + ".todo1_id", todo1_id + "");
				}

				
				tagsWatchList = db.getAllToDosByTag(modelName);
				list.clear();
				for (Todo todo : tagsWatchList) {
					friendsSDetails = new MyFriendsSDetails();
					Log.d("ToDo " + modelName, todo.getNote());
					friendsSDetails.setMyfriendname(todo.getNote());
					Log.i("Status", todo.getStatus() + "");
					friendsSDetails.setMyfriendnickname(todo.getStatus() + "");
					friendsSDetails.setPhoto(android.R.drawable.ic_menu_call);
					list.add(friendsSDetails);
				}

				adapter = new MyListAdapter(contex, list);
				listView.setAdapter(adapter);
				listView.setMultiChoiceModeListener(this);
				listView.setChoiceMode(listView.CHOICE_MODE_MULTIPLE_MODAL);
			}
			if (resultCode == RESULT_CANCELED) {
				// Write your code if there's no result
			}
		}
	}

	protected void showCustomDialog() {
		// TODO Auto-generated method stub

		final Dialog dialog = new Dialog(ContactActivity.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.custom_dialog);

		final EditText editText = (EditText) dialog.findViewById(R.id.name);

		final EditText editText2 = (EditText) dialog
				.findViewById(R.id.contactNo);

		Button button = (Button) dialog.findViewById(R.id.saveId);
		button.setOnClickListener(new View.OnClickListener() {
			Todo todo;

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				todo = new Todo();
				todo.setNote(editText.getText().toString());
				todo.setStatus(Long.parseLong(editText2.getText().toString()));
				long todo1_id = db.createToDo(todo,
						new long[] { Long.parseLong(modelID) });
				dialog.dismiss();
			}
		});

		dialog.show();
	}

	@Override
	public boolean onActionItemClicked(ActionMode arg0, MenuItem arg1) {
		switch (arg1.getItemId()) {
		case R.id.delete:
			SparseBooleanArray selected = adapter.getSelectedIds();
			for (int i = (selected.size() - 1); i >= 0; i--) {
				if (selected.valueAt(i)) {
					MyFriendsSDetails selecteditem = adapter.getItem(selected
							.keyAt(i));
					db.deleteToDo(selecteditem.getTodo_Id());
					db.deleteTodoWithTag(selecteditem.getTodo_Id(), Long.parseLong(modelID));
					adapter.remove(selecteditem);
				}
			}
			// Close CAB
			arg0.finish();
			return true;
		default:
			return false;
		}
	}

	@Override
	public boolean onCreateActionMode(ActionMode arg0, Menu arg1) {
		arg0.getMenuInflater().inflate(R.menu.main, arg1);
		return true;
	}

	@Override
	public void onDestroyActionMode(ActionMode arg0) {
		adapter.removeSelection();
	}

	@Override
	public boolean onPrepareActionMode(ActionMode arg0, Menu arg1) {
		return false;
	}

	@Override
	public void onItemCheckedStateChanged(ActionMode arg0, int arg1, long arg2,
			boolean arg3) {

		final int checkedCount = listView.getCheckedItemCount();
		arg0.setTitle(checkedCount + " Selected");
		adapter.toggleSelection(arg1);

	}

}
