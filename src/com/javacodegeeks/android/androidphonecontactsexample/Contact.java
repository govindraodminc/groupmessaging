package com.javacodegeeks.android.androidphonecontactsexample;

import java.util.ArrayList;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

public class Contact implements Parcelable {

	String name;
	String ph_no;
	private ArrayList<Contact> items;

	public Contact() {
		// TODO Auto-generated constructor stub
	}

	public Contact(String name, String ph_no) {
		super();
		this.name = name;
		this.ph_no = ph_no;
	}

	public String getPh_no() {
		return ph_no;
	}

	public void setPh_no(String ph_no) {
		this.ph_no = ph_no;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return this.name;
	}

	public ArrayList<Contact> getItems() {
		return items;
	}

	public void setItems(ArrayList<Contact> items) {
		this.items = items;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int arg1) {
		// TODO Auto-generated method stub
		dest.writeString(name);
		dest.writeString(ph_no);
		Bundle b = new Bundle();
		b.putParcelableArrayList("items", items);
		dest.writeBundle(b);
	}

	public static final Parcelable.Creator<Contact> CREATOR = new Parcelable.Creator<Contact>() {
		public Contact createFromParcel(Parcel in) {
			Contact contact = new Contact();
			contact.name = in.readString();
			contact.ph_no = in.readString();
			
			Bundle b = in.readBundle(Contact.class.getClassLoader());
			contact.items = b.getParcelableArrayList("items");

			return contact;
		}

		@Override
		public Contact[] newArray(int size) {
			return new Contact[size];
		}
	};
}
