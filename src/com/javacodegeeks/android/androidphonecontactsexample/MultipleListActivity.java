package com.javacodegeeks.android.androidphonecontactsexample;

import java.util.ArrayList;
import com.example.contactgroup.R;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;

public class MultipleListActivity extends Activity implements OnClickListener {
	ListView mListView;
	Button btnShowCheckedItems;
	ArrayList<Contact> mContacts;
	MultiSelectionAdapter<Contact> mAdapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.multiplelistcctivity_layout);
		bindComponents();
		fetchContacts();
		addListeners();
	}

	private void bindComponents() {
		// TODO Auto-generated method stub
		mListView = (ListView) findViewById(android.R.id.list);
		btnShowCheckedItems = (Button) findViewById(R.id.btnShowCheckedItems);
		mContacts = new ArrayList<Contact>();
	}

	private void addListeners() {
		// TODO Auto-generated method stub
		btnShowCheckedItems.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (mAdapter != null) {
			ArrayList<Contact> mArrayContacts = mAdapter.getCheckedItems();
			Intent intent = new Intent();
			Bundle b = new Bundle();
			b.putParcelableArrayList("contacts", mArrayContacts);
			intent.putExtras(b);
			setResult(RESULT_OK, intent);
			finish();
		}
	}

	public void fetchContacts() {

		String phoneNumber = null;
		String email = null;

		Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
		String _ID = ContactsContract.Contacts._ID;
		String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
		String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;

		Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
		String Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
		String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;

		Uri EmailCONTENT_URI = ContactsContract.CommonDataKinds.Email.CONTENT_URI;
		String EmailCONTACT_ID = ContactsContract.CommonDataKinds.Email.CONTACT_ID;
		String DATA = ContactsContract.CommonDataKinds.Email.DATA;

		StringBuffer output = new StringBuffer();
		ContentResolver contentResolver = getContentResolver();
		Cursor cursor = contentResolver.query(CONTENT_URI, null, null, null,
				null);

		// Loop for every contact in the phone
		if (cursor.getCount() > 0) {
			while (cursor.moveToNext()) {
				String contact_id = cursor
						.getString(cursor.getColumnIndex(_ID));
				String name = cursor.getString(cursor
						.getColumnIndex(DISPLAY_NAME));
				int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor
						.getColumnIndex(HAS_PHONE_NUMBER)));
				if (hasPhoneNumber > 0) {
					// output.append("\n First Name:" + name);
					Log.i(getClass().getSimpleName(), "" + name);
					// Query and loop for every phone number of the contact
					Cursor phoneCursor = contentResolver.query(
							PhoneCONTENT_URI, null, Phone_CONTACT_ID + " = ?",
							new String[] { contact_id }, null);

					while (phoneCursor.moveToNext()) {
						phoneNumber = phoneCursor.getString(phoneCursor
								.getColumnIndex(NUMBER));
						// output.append("\n Phone number:" + phoneNumber);
						Log.i(getClass().getSimpleName(), "" + phoneNumber);
					}
					phoneCursor.close();
					mContacts.add(new Contact(name, phoneNumber));

					// Query and loop for every email of the contact
					/*
					 * Cursor emailCursor =
					 * contentResolver.query(EmailCONTENT_URI, null,
					 * EmailCONTACT_ID+ " = ?", new String[] { contact_id },
					 * null); while (emailCursor.moveToNext()) { email =
					 * emailCursor.getString(emailCursor.getColumnIndex(DATA));
					 * //output.append("\nEmail:" + email);
					 * Log.i(getClass().getSimpleName() , ""+email); }
					 * emailCursor.close();
					 */
				}
				// output.append("\n");
			}
			// outputText.setText(output);
		}
		mAdapter = new MultiSelectionAdapter<Contact>(this, mContacts);
		mListView.setAdapter(mAdapter);
	}

}
